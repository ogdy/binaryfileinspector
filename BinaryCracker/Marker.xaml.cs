﻿using System.Windows.Controls;

namespace Defacto.Backoffice.BinaryCracker {
    /// <summary>
    /// Logique d'interaction pour Marker.xaml
    /// </summary>
    public partial class Marker : Border
    {
        public Marker(string text)
        {
            InitializeComponent();
            label.Text = text;
        }
    }
}
